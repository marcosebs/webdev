<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Excel;

class Export extends Model
{
    public static function getData()
    {
        $data = array(
            array(
            'im' => 'Category',
            'name' => '123123',
            'category' => '',
            'free_shipping' => '',
            'description' => '',
            'price' => '',
            ),
            array(
            'im' => '',
            'name' => '',
            'category' => '',
            'free_shipping' => '',
            'description' => '',
            'price' => '',
            ),
            array(
            'im' => '',
            'name' => '',
            'category' => '',
            'free_shipping' => '',
            'description' => '',
            'price' => '',
            ),
            array(
            'im' => 'im',
            'name' => 'name',
            'category' => 'category',
            'free_shipping' => 'free_shipping',
            'description' => 'description',
            'price' => 'price',
            ),
            array(
            'im' => '1001',
            'name' => 'Furadeira X',
            'category' => 'Ferramentas',
            'free_shipping' => 0,
            'description' => 'Furadeira Cinza',
            'price' => '1.00',
            )
        );

        return Excel::create('file', function($excel) use($data) {
            $excel->sheet('Plan1', function($sheet) use($data) {
                //Create rows
                $sheet->rows($data);
            });
        })->export('xls');
    }
}
