<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public $timestamps = false;

    protected $fillable = [ 'im',
                						'name',
                						'category',
                						'free_shipping',
                						'description',
                						'price'
                					   ];

}
