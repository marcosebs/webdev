<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Excel;
use App\Product;

class SaveExcel implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private $archive;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $archive)
    {
        $this->archive = $archive;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //foreach in the array of the input
        foreach ($this->archive as $p => $arrRows) {
            //foreach in the rows products
            foreach ($arrRows as $row){
                //first array in method checks the fields, if exists updates, else create
                Product::updateOrCreate([
                                          'im' => $row[0],
                                          'name' => $row[1]
                                        ],
                                        [
                                          'category' => $row[2],
                                          'free_shipping' => $row[3],
                                          'description'=> $row[4],
                                          'price' => $row[5]
                                        ]);
            }
        }
    }
}
