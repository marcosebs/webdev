<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Product;
use Excel;
use Session;
use App\Jobs\SaveExcel;

class ProductController extends Controller
{
     /**
      * Return main page
      */
    public function index()
    {
        return view('template.index');
    }

    /**
     * List products from database
     */
    public function listImportedProducts()
    {
        $products = Product::all();
        return view('product.list')
            ->with('products', $products);
    }

    /**
     * Import sheet from file xlsx
     */
    public function importSheet()
    {
        if (Input::hasFile('sheet')) {

            //set file in variable
      	    $archive = Input::file('sheet')->getRealPath();

            //Method of the class Excel for content load UTF-8
            $product = Excel::load($archive, 'UTF-8');

            //Methods for ignore basic heading, null cells and skip rows
            $product->noHeading();
            $product->skipRows(3);
            $product->ignoreEmpty();

            //Fires the queue
            $this->dispatch(new SaveExcel($product->toArray()));

            //Flash message from queue load
      	    Session::flash('message', 'Processando importação dos produtos... ');

  		      return back();
        }

      //Flash for error on import sheet
      Session::flash('message', 'Por favor, insira um arquivo xlsx válido... ');

      return back();
    }
}
