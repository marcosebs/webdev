<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */
 
namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use App\Product;

class ProductTest extends TestCase
{
    /**
     * Construct with Mockery
     */
    public function __construct()
    {
        $this->mock = Mockery::mock(Product::class);
    }

    /**
     * function for close mockery
     */
    public function tearDown()
    {
        Mockery::close();
    }

    /*
     * test method that it has a function of list all products
     */
    public function testMethodAll()
    {
        $product = Product::all();

        $this->mock = Mockery::mock(Product::class);

        $this->mock->shouldReceive('all')->andReturn($product);

        $this->app->instance(Product::class, $this->mock);

        $this->call('GET', 'products');

        $this->assertNotNull(true, $this->mock);
    }

  /**
   * test method update or create in the model products with mock
   */
    public function testUpdateOrCreateProductMock()
    {
        $product = [
            'im' => '1234',
            'name' => 'Product test',
            'category' =>  'Test',
            'free_shipping' => '1',
            'description' => 'Product test',
            'price' => '1.00'
          ];

        $this->mock->shouldReceive('updateOrCreate')->once()->with($product);

        $this->app->instance(Product::class, $this->mock);

        $this->assertNotNull(true, $this->mock);
  }



}
