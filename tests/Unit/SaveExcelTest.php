<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Export;
use Excel;
use App\Jobs\SaveExcel;

class SaveExcelTest extends TestCase
{
    /**
     * Test queue, import file xls
     *
     * @return void
     */
    public function testQueueImportSheet()
    {
       $exc = Export::getData();

        $job = new SaveExcel();

        $this->expectsJobs($job->dispatch($exc));
    }
}
