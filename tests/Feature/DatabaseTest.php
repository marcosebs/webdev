<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Product;

class DatabaseTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSaveDabatase()
    {
      Product::create([
                        'im' => '1234',
                        'name' => 'Product test',
                        'category' =>  'Test',
                        'free_shipping' => '1',
                        'description' => 'Product test',
                        'price' => '1.00'
                      ]);
                      
      $this->assertDatabaseHas('products', ['name' => 'Product test']);
    }

    /**
     * Test update or/and create in database and check if exists
     */
    public function testUpdateOrCreateDabatase()
    {
      Product::updateOrCreate([
                                'im' => '1234',
                                'name' => 'Product test2'
                              ],
                              [
                                'category' =>  'Test',
                                'free_shipping' => '1',
                                'description' => 'Product test2',
                                'price' => '1.00'
                              ]);

      $this->assertDatabaseHas('products', ['name' => 'Product test2']);
    }

    /**
     * test for see tables in database
     */
    public function testSeeTables()
    {
        $schema = $this->app['db']->connection()->getSchemaBuilder();
       //Array of jobs and products
        $tables = [
                   'jobs' => [
                               'id',
                               'queue',
                               'payload',
                               'attempts',
                               'reserved_at',
                               'available_at',
                               'created_at'
                             ],
                  'products' => [
                                   'id',
                                   'name',
                                   'category',
                                   'free_shipping',
                                   'description',
                                   'price',
                                   'im'
                                ],
                  ];

        foreach ($tables as $table => $columns) {
            $this->assertTrue($schema->hasTable($table), sprintf(
                'Tabela não existente.'
            ));
            foreach ($columns as $col) {
                $this->assertTrue(
                    $schema->hasColumn($table, $col),
                    sprintf('Coluna não existente')
                );
            }
        }
    }
}
