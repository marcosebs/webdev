<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use App\Product;

class ProductsControllerTest extends TestCase
{
    /**
     * A basic test for check http request.
     *
     * @return void
     */
    public function testProductsRequest()
    {
        $productView = $this->call('GET', '/products');
        $productView->assertViewHas('products')
          ->assertStatus(200);
    }

    /**
     * A basic test for check http request.
     *
     * @return void
     */
    public function testIndexRequest()
    {
        $indexView = $this->call('GET', '/');
        $indexView->assertStatus(200);
    }

    /**
     * Instance mock in the product class
     */
    public function __construct()
    {
        $this->mock = Mockery::mock('Eloquent', Product::class);
    }

    /**
     * function for close mockery
     */
    public function tearDown()
    {
        Mockery::close();
    }

    /**
     * test method all in the model products with mock
     */
    public function testProductsRequestMock()
    {
        $product = Product::all();

        $this->mock->shouldReceive('all')->once()->andReturn($product);

        $this->app->instance(Product::class, $this->mock);
        $view = $this->call('GET', 'products');

        $view->assertViewHas('products')
           ->assertStatus(200);
    }


}
