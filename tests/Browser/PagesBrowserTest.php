<?php
/**
 * @Author: Marcos Eduardo B. Santos
 * @Date:   2017-04-18
 */

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PagesBrowserTest extends DuskTestCase
{
    /**
     * A basic browser test for see link for products list.
     *
     * @return void
     */
    public function testLinkForProducts()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->clickLink('Produtos')
                    ->assertPathIs('/products');
        });
    }

    /**
     * Basic test for check button import and redirect on click
     */
    public function testHeaderTableProducts()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/products')
                  ->press('Importar')
                  ->assertPathIs('/products')
                  ;
        });
    }

}
