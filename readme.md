## Sobre

Teste WebDev. Aplicação para importação (criar/atualizar) registros de produtos com base em planilha xls, utilizando thread para execução da tarefa. 

## Requisitos do servidor p/ estrutura Laravel

PHP >= 5.5.9

OpenSSL PHP Extension

PDO PHP Extension

Mbstring PHP Extension

Tokenizer PHP Extension

## Lib's Utilizadas

Laravel Excel:
http://www.maatwebsite.nl/laravel-excel/docs

Laravel Dusk:
https://laravel.com/docs/5.4/dusk

## Como Instalar

cd /var/www

mkdir webdev

git init

git remote add origin git@bitbucket.org:marcosebs/webdev.git

git pull origin master

composer install

configurar acesso ao banco (arquivo .env)

php artisan migrate