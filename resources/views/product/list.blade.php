@extends('template.index')

@section ('content')
	
	<div class="container">
	    <div class="row">
        	<div class="col-md-8 col-md-offset-2">
            	<div class="panel panel-default">
            		@if(session('message'))
	                    <div class="alert alert-info">
	                        <strong>{{session('message')}}</strong>
	                    </div>
                	@endif
                	@if(session('messageError'))
	                    <div class="alert alert-danger">
	                        <strong>{{session('messageError')}}</strong>
	                    </div>
                	@endif
                	<div class="panel-heading">
                	Importar Excel
                	</div>

                	<div class="container">

						<form action="{{ url('importedSheet') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
						{!! method_field('patch') !!}
							<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
							<br />
							<input type="file" name="sheet" />
							<br />
							<button class="btn btn-primary">Importar</button>
						</form>

					</div>

					<div class="panel panel-default">
						<div class="container">
							<table class="table table-responsive" style="width: 700px; overflow: auto;">
								<thead >
									<tr>
										<td><b>Im</b></td>
										<td><b>Name</b></td>
										<td><b>Category</b></td>
										<td><b>Free Shipping</b></td>
										<td><b>Description</b></td>
										<td><b>Price</b></td>
									</tr>	
								</thead>
								<tbody >
									@foreach ($products as $p)
									<tr>
										<td>{{$p->im}}</td>
										<td>{{$p->name}}</td>
										<td>{{$p->category}}</td>
										<td>{{$p->free_shipping}}</td>
										<td>{{$p->description}}</td>
										<td>{{$p->price}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>		
	</div>

@endsection