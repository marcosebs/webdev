<html>

    <head>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <meta charset="utf-8">

        <title>WebDev</title>

    </head>

    <body>

        <div class="container">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('products') }}"><strong>Produtos</strong></a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            @yield('content')

        </div>

    </body>

</html>
