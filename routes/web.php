<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//home
Route::get('/', 'ProductController@index');
//list products and search file for import
Route::get('products', 'ProductController@listImportedProducts')->name('products');
//process imports file
Route::match(['patch', 'post'], '/importedSheet',  'ProductController@importSheet');
